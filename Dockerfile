FROM php:8.1.3-fpm

# Install useful tools and important libraries
RUN apt-get update && apt-get -y install apt-utils nano wget dialog vim libcurl4 libcurl4-openssl-dev zlib1g-dev libzip-dev zip libbz2-dev locales libmcrypt-dev libicu-dev libonig-dev libxml2-dev

# Install system dependencies and PHP extensions required for Laravel
RUN apt-get install -y libpng-dev libjpeg-dev libfreetype6-dev zip unzip \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd pdo pdo_mysql bz2 intl

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Set the working directory in the container
WORKDIR /var/www/html

# Copy the Laravel application files into the container
COPY ./src /var/www/html/

EXPOSE 8000

# The CMD command specifies the command that will be executed when the container starts.
# Here, we start the PHP-FPM server to serve the Laravel application.
CMD ["php-fpm"]
