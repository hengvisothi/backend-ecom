<?php

namespace App\Http\Controllers;

use App\Models\product;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    public function store(Request $request){
        $request -> validate([
            'title' => 'required',
            'category' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'required'

        ]);
        // dd($request->input());
        $product = new Product();
        $product->title = $request->title;
        $product->category= $request->category;
        $product->description= $request->description;
        $product->price = $request->price;
        //image
        $extension = $request->file('image')->extension();
        $filename = date('YmdHis').'.'.$extension; //20230720203055.jpg 
        $request->file('image')->move(public_path('uplaods/'), $filename);
        $product->image = $filename;
        $product->save();
        return redirect()-> route('products.create');
        // dd($request->input());
    }
    public function edit($id){
        $product = Product::where("id", $id)->first();
     
        return view("products.edit", compact('product'));

     
    }
    public function delete($id){
        $product = Product::where("id", $id)->first();

        if(file_exists(public_path('uploads/' .$product->image)) AND !empty($product->image)){
            unlink(public_path('uploads/' .$product->image));
        }
        $product->delete();
        return redirect()-> route('products.show');

     
    }
    public function show(){
        $products = Product::orderBy("id", "desc")->get();

        return view("products.show", compact('products'));
    }

    public function update($id, Request $request){
        $request -> validate([
            'title' => 'required',
            'category' => 'required',
            'description' => 'required',
            'price' => 'required'
            // 'image' => 'required'

        ]);
        $product = Product::where("id", $id)-> first();
        if($request->hasFile('image')){
            $request -> validate([
                'image' => 'required'
            ]);
            if(file_exists(public_path('uploads/' .$product->image)) AND !empty($product->image)){

                unlink(public_path('uploads/' .$product->image));
            }
                   //image
            $extension = $request->file('image')->extension();
            $filename = date('YmdHis').'.'.$extension; //20230720203055.jpg 
            $request->file('image')->move(public_path('uplaods/'), $filename);
            $product->title = $request->title;
            $product->category= $request->category;
            $product->description= $request->description;
            $product->price = $request->price;
            $product->image = $filename;
            $product->update();
        }
        // dd($request->input());
        $product->title = $request->title;
        $product->category= $request->category;
        $product->description= $request->description;
        $product->price = $request->price;
        $product->update();
        return redirect()-> route('products.show');

    }


}
